const stripe = require('stripe')('sk_test_z798gfPe2Q9hXUN5gcGMPq6Z');

(async function () {
    const proration_date = Math.floor(Date.now() / 1000);

    const subscription = await stripe.subscriptions.retrieve('sub_IqP2YAMpmNtR3B');

    const items = [{
        id: subscription.items.data[0].id,
        price: 'price_1IBSYDA0zX6XkXax5hKgslC4', // Switch to new price
    }];

    const invoice = await stripe.invoices.retrieveUpcoming({
        customer: 'cus_IqP1VDZpJI1tBY',
        subscription: 'sub_IqP2YAMpmNtR3B',
        subscription_items: items,
        subscription_proration_date: proration_date,
    });

    console.log(invoice.total / 100);
})();
